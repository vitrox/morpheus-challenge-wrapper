/*
 * morpheus-challenge-wrapper
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcw.challenges

import com.gitlab.vitrox.mc.{Challenge, Helper}
import com.gitlab.vitrox.mcw.KList

/** Introduction video (german): https://www.youtube.com/watch?v=6i92MxAgLsI
  * Solution video (german): https://www.youtube.com/watch?v=5Z-E8JLyPxQ
  */
abstract class CC7 extends Challenge[KList[Int, Int], Product2[Int, Int]] {
  override def challengeEndpoint = "7"
  override def solutionEndpoint = "7"

  override protected def parseChallenge(input: java.io.InputStream): KList[Int, Int] =
    mapper.readValue(input, classOf[KList[Int, Int]])
  override protected def sendSolution(o: Product2[Int, Int], output: java.io.OutputStream): Unit =
    Helper.writeStringToOutput(Helper.constructToken(mapper.writeValueAsString(o), quotationMarks = false), output)
}