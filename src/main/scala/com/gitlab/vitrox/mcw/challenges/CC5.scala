/*
 * morpheus-challenge-wrapper
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcw.challenges

import com.gitlab.vitrox.mc.{Challenge, Helper}

/** Introduction video (german): https://www.youtube.com/watch?v=QXD5otIXBqQ
  * Solution video (german): https://www.youtube.com/watch?v=7maywQi1kO0
  *
  * Challenge is broken, the solution is not correct sometimes, that is caused by multiple bugs on the server backend
  * for more information see https://stackoverflow.com/questions/7604966/maximum-and-minimum-values-for-ints
  * or https://docs.python.org/3/tutorial/floatingpoint.html
  * TLDR: The default server backend is running python flask,
  *       the solution is casted to an int, which is unbounded in python version >= 3
  */
abstract class CC5 extends Challenge[Iterator[String], Long] {
  override def challengeEndpoint: String = "5"
  override def solutionEndpoint: String = "5"

  override protected def parseChallenge(input: java.io.InputStream): Iterator[String] =
    Helper.inputStreamAsString(input).split(' ').toIterator
  override protected def sendSolution(o: Long, output: java.io.OutputStream): Unit =
    Helper.writeStringToOutput(Helper.constructToken(o, quotationMarks = false), output)
}