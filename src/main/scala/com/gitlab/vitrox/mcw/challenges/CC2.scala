/*
 * morpheus-challenge-wrapper
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcw.challenges

import com.gitlab.vitrox.mc.{Challenge, Helper}
import com.gitlab.vitrox.mcw.KList

/** Introduction video (german): https://www.youtube.com/watch?v=GldayZtzes0
  * Solution video (german): https://www.youtube.com/watch?v=6WTIh6rI2XA
  */
abstract class CC2 extends Challenge[KList[Long, Long], Int] {
  override def challengeEndpoint: String = "2"
  override def solutionEndpoint: String = "2"

  override protected def parseChallenge(input: java.io.InputStream): KList[Long, Long] =
    mapper.readValue(input, classOf[KList[Long, Long]])
  override protected def sendSolution(o: Int, output: java.io.OutputStream): Unit =
    Helper.writeStringToOutput(Helper.constructToken(o, quotationMarks = false), output)
}