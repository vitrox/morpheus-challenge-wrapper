/*
 * morpheus-challenge-wrapper
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcw

import com.gitlab.vitrox.mc.Challenge
import org.scalatest.{FlatSpec, Matchers}

class ChallengesSpec extends FlatSpec with Matchers {

  @inline private def checkConnection(challenge: Challenge[_, _]): Unit = {
    challenge.getClass.getSimpleName should "check connection" in {
      challenge.flag shouldBe a [Option[_]]
    }
  }

  checkConnection(CC1Impl)
  checkConnection(CC2Impl)
  checkConnection(CC2SortedImpl)
  checkConnection(CC3Impl)
  checkConnection(CC4Impl)
  checkConnection(CC5Impl)
  checkConnection(CC6Impl)
  checkConnection(CC7Impl)
  checkConnection(CC8Impl)

}