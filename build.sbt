ThisBuild / organization := "com.gitlab.vitrox"
ThisBuild / scalaVersion := "2.12.6"
ThisBuild /    resolvers += "jitpack" at "https://jitpack.io"

lazy val root = (project in file("."))
  .settings(
    name                := "morpheus-challenge-wrapper",
    version             := "0.8.0",
    libraryDependencies ++= Seq(
      "com.gitlab.vitrox" % "morpheus-challenge" % "0.1.1",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.7",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    )
  )