# morpheus-challenge-wrapper (mcw)

The challenge api contains tasks which should be solved in a programmatic way. 
This library wraps around [morpheus-challenge](https://gitlab.com/vitrox/morpheus-challenge), it handles all the 
connections and parsing so that you need only to include your solution algorithm.

## Implementation

### Dependencies

Uses the JSON parser [Jackson](https://github.com/FasterXML/jackson-module-scala). 

### Installation

Use JitPack to add this project into your build.

#### sbt

Add the JitPack repository in your build.sbt at the end of resolvers:

```scala
resolvers += "jitpack" at "https://jitpack.io"
```

Add the dependency:

```scala
libraryDependencies += "com.gitlab.vitrox" % "morpheus-challenge-wrapper" % version
```

Other JVM languages than Scala were not tested.

See [this](https://jitpack.io/#com.gitlab.vitrox/morpheus-challenge-wrapper) guide on JitPack for more information 
or click on the badge.

### Usage

Implement an object with their challenge abstraction.

You just need include your solution in the implemented function solutionAlgorithm.

```scala
object CC1Impl extends CC1 {
  // Not a valid solution for the first challenge
  override protected def solutionAlgorithm(i: String): String = ""
}
```

## Credits

Huge thanks to the german youtuber "[TheMorpheus407](https://www.youtube.com/user/TheMorpheus407)" and his community
for these challenges.